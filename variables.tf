variable "env" {
  type = map
  description = "This is the map holding values per environment. dev, sit, uat, prd"
  default = {
    "dev" = { 
      "bucket_name" = "my-dev-bucket"
      "count"       = "1"
    }
    "sit" = {
      "bucket_name" = "my-sit-bucket"
      "count"       = "1"
    }
  }
}
