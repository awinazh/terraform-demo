terraform {                                      
  required_providers {                           
    aws = {                                      
      source = "hashicorp/aws"                   
    }                                            
  }                                              
  required_version = ">= 0.12"                   
}                                                
                                                 
provider "aws" {                                 
  access_key                  = "mock_access_key"
  region                      = "us-east-1"      
  s3_use_path_style           = true             
  secret_key                  = "mock_secret_key"
  skip_credentials_validation = true             
  skip_metadata_api_check     = true             
  skip_requesting_account_id  = true             
                                                 
  endpoints {                                    
    apigateway     = "http://aws.local:4566"
    cloudformation = "http://aws.local:4566"
    cloudwatch     = "http://aws.local:4566"
    dynamodb       = "http://aws.local:4566"
    es             = "http://aws.local:4566"
    firehose       = "http://aws.local:4566"
    iam            = "http://aws.local:4566"
    kinesis        = "http://aws.local:4566"
    lambda         = "http://aws.local:4566"
    route53        = "http://aws.local:4566"
    redshift       = "http://aws.local:4566"
    s3             = "http://aws.local:4566"
    secretsmanager = "http://aws.local:4566"
    ses            = "http://aws.local:4566"
    sns            = "http://aws.local:4566"
    sqs            = "http://aws.local:4566"
    ssm            = "http://aws.local:4566"
    stepfunctions  = "http://aws.local:4566"
    sts            = "http://aws.local:4566"
  }                                              
}                                                
                                                 
resource "aws_s3_bucket" "b" {                   
  count  = var.env[terraform.workspace].count
  bucket = "${var.env[terraform.workspace].bucket_name}.${count.index}"                        
}
