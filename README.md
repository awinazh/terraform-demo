# Terraform Demo

A simple example of setting up [localstack](https://github.com/localstack/localstack) via docker compose, provisioning a bucket via [terraform](https://www.terraform.io/).

---

## Pre-requisite to run demo locally

1. Git
2. Docker
3. Docker Compose

---

## Clone a repository

`git clone https://bitbucket.org/awinazh/terraform-workspace-demo.git`

---

## Run docker compose

`cd docker`

`docker-compose up -d`

---

## Configure aws for localstack

run following commands..

`$` `docker exec -it iac bash`

`$` `aws configure`

enter dummy details

````
AWS Access Key ID       : dummy
AWS Secret Access Key   : dummy
Default region name     : eu-east-1
Default output format   : json
````

`$` `exit`

---

## Open terraform terminal

run following command to setup basic things..

`$` `docker exec -it iac bash`

`$` `terraform init`

`$` `terraform workspace new dev`

`$` `terraform workspace new sit`

create aws bucket on localstack

`$` `terraform apply`

`$` `exit`

---

## Check if bucket is created

`$` `docker exec -it iac bash`

`$` `aws s3 ls --endpoint-url=http://aws.local:4566`

must print something like..

`2022-03-31 20:23:17 my-sit-bucket.0`

---

## Shutdown containers

`$` `docker-compose down`